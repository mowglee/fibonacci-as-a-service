# Fibonacci as a Service


| Method | path | parameter | description |
| --- | --- | --- | --- |
| GET | [http://localhost:3000/api/fibonacci/{n}](http://localhost:3000/api/fibonacci/{n}) | n: number [1, 1 000 000 ] | returns the n-th Fibonacci number |

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e
```

## Docker

```bash
# build image
docker build -t fibonacci .
# run image locally
docker run fibonacci -p 3000:3000
```

To run the service with the monitoring stack, run `docker compose up`.
Go to `http://localhost:3001` to see the live metrics in Grafana.

## Configuration

To configure the service create a .env file in the root folder with the following values or set the values in your environment.

```
PORT=3000
MAX_FIBONACCI=1000000
CACHE_TTL=0
CACHE_MAX=1000000
```
---
## Metrics

The service provides a `/metrics` route to expose Prometheus metrics.
The service exposes [default metrics](https://prometheus.io/docs/instrumenting/writing_clientlibs/#standard-and-runtime-collectors) as well as custom metrics.

### Custom metrics
| Name | Type | Description |
| --- | --- | --- |
| request_duration | Histogramm | to determine throughput and request latency |
| error_counter | Counter | to determine the error rate |

These metrics are visualized in a Grafana dashboard that is accessible via `http://localhost:3001` after spinning up docker compose via `docker compose up`.

![grafana dashboard](./doc/dashboard.png)

## Performance

The application uses the [fast doubling algorithm](https://www.nayuki.io/page/fast-fibonacci-algorithms) to calculate Fibonacci numbers.
This algorithm has a time complexity of O(log n).

Unfortunately, [Binet's formula](https://mathworld.wolfram.com/BinetsFibonacciNumberFormula.html) to calculate the n-th Fibonacci number in a constant time could not be used, since Javascript can not handle arithmetics with large numbers and rational numbers.

To increase performance the service uses basic In-memory caching. The benefit of using the cache can be seen clearly in the decreasing response time in the above dashboard.

In addition to this, the fastest nodejs server [fastify](https://www.fastify.io/) is used to handle as many concurrent requests as possible.

The actual performance was tested with the [artillery](https://artillery.io/) performance test library on a MacBook Pro while the service was running in a local Docker container.

The result of the load test heavily depends on the value of the parameter *n*. The result shown below is achieved with random *n* ranging from 1 to 100 000.

> I would highly recommend setting a maximum allowed value of n (to e.g. 1 000 000), otherwise, the service will be very vulnerable. If this does not meet the business requirements, another approach has to be taken (e.g. implementing Binet's formula in another programming language).

```
--------------------------------
Summary report @ 20:32:17(+0100)
--------------------------------

vusers.created_by_name.Get n-th fibonacci number: ........... 125344
vusers.created.total: ....................................... 125344
vusers.completed: ........................................... 125344
vusers.session_length:
  min: ...................................................... 2.8
  max: ...................................................... 1716
  median: ................................................... 13.9
  p95: ...................................................... 80.6
  p99: ...................................................... 699.4
http.request_rate: .......................................... 128/sec
http.requests: .............................................. 125344
http.codes.200: ............................................. 125344
http.responses: ............................................. 125344
http.response_time:
  min: ...................................................... 1
  max: ...................................................... 1708
  median: ................................................... 12.1
  p95: ...................................................... 67.4
  p99: ...................................................... 699.4
  ```

## SLI’s/SLO’s

| SLI | Description | Metric |
| --- | --- |--- |
| Availability | The proportion of requests that resulted in a successful response. | `(request_duration_count - error_counter)/request_duration_count` |
| Latency | The proportion of requests that were faster than some treshold. | `histogram_quantile(0.95, sum(rate(request_duration_bucket[1m])) by (le))` |

> The Downside of using server-side metrics is, that we have no information about requests that didn't even reach the backend. It would be better to utilize metrics from an Ingress gateway or a loadbalancer. 

To have room for an error budged, I would propose lower SLOs than the SLI that was measured in the performance test. I would set the SLO on a rolling 7-day window, to make quick fixes if the target is missed.

| SLO | Objective |
| --- | --- |
| Availability | 98% |
| Latency | 90% < 200ms |
| Latency | 95% < 600ms |

The SLOs should be refined if more data from production is available.

## Scaling
Since node is single-threaded, the service should be scaled horizontally on memory usage.

The currently used in-memory cache should then be replaced by a shared cache, e.g. Redis.

## Possible improvements:
- utilize Redis for caching
- add memoization to fast doubling algorithm
- implement horizontal autoscaling
