import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect(
        'Fibonacci as a Service\n call /api/fibonacci/{num} to get the n-th Fibonacci number',
      );
  });

  describe('fibonacci api', () => {
    it('/api/fibonacci/2 (GET) should return n-th fibonacci number', () => {
      return request(app.getHttpServer())
        .get('/api/fibonacci/2')
        .expect(200)
        .expect('1');
    });

    it('/api/fibonacci/-1 (GET) should return a bad request http code', () => {
      return request(app.getHttpServer()).get('/api/fibonacci/-1').expect(400);
    });
  });
});
