FROM node:16-alpine as BUILDER

WORKDIR /usr/app

COPY . ./

RUN yarn install
RUN yarn build
# remove dev dependencies
RUN rm -rf node/modules
RUN yarn install --production --frozen-lockfile

FROM node:16-alpine

WORKDIR /usr/app

# copy ts files for sourcemap support
COPY --from=BUILDER /usr/app/src ./src/

# copy transpiled javascript and node modules
COPY --from=BUILDER /usr/app/dist ./dist/
COPY --from=BUILDER /usr/app/package.json ./
COPY --from=BUILDER /usr/app/node_modules ./node_modules/


EXPOSE 3000

CMD [ "yarn", "start:prod" ]
