import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getInfo(): string {
    return 'Fibonacci as a Service\n call /api/fibonacci/{num} to get the n-th Fibonacci number';
  }
}
