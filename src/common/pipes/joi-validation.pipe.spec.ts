import { BadRequestException } from '@nestjs/common';
import * as Joi from 'joi';
import { JoiValidationPipe } from './joi-validation.pipe';

describe('JoiValidationPipe', () => {
  it('should be defined', () => {
    expect(new JoiValidationPipe(Joi.number().min(0))).toBeDefined();
  });

  it('should return value if schema validation succeeds', () => {
    expect(
      new JoiValidationPipe(Joi.number().min(0)).transform(2, {
        type: 'param',
      }),
    ).toBe(2);
  });

  it('should return BadRequestException if schema validation fails', () => {
    expect(() =>
      new JoiValidationPipe(Joi.number().min(0))
        .transform(-1, {
          type: 'param',
        })
        .toBe(new BadRequestException()),
    );
  });
});
