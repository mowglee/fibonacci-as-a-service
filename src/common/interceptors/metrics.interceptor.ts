import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { InjectMetric } from '@willsoto/nestjs-prometheus';
import { Histogram } from 'prom-client';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class MetricsInterceptor implements NestInterceptor {
  constructor(
    @InjectMetric('request_duration') public latency: Histogram<any>,
  ) { }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const stop = this.latency.startTimer();
    return next.handle().pipe(tap(() => stop()));
  }
}
