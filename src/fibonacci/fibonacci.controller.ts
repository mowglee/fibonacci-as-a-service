import {
  CacheInterceptor,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import * as Joi from 'joi';
import { ErrorsInterceptor, MetricsInterceptor } from '../common/interceptors';
import { JoiValidationPipe } from '../common/pipes/joi-validation.pipe';
import { FibonacciService } from './fibonacci.service';

const MAX = parseInt(process.env.FIBONACCI_MAX) || 100000;

@Controller('api/fibonacci')
@UseInterceptors(MetricsInterceptor, CacheInterceptor, ErrorsInterceptor)
export class FibonacciController {
  constructor(private readonly fibonacciSvc: FibonacciService) { }

  @Get(':num')
  @UsePipes(new JoiValidationPipe(Joi.number().min(0).max(MAX)))
  fibonacci(@Param('num', ParseIntPipe) num: number) {
    return this.fibonacciSvc.getFibonacci(num);
  }
}
