import { Test, TestingModule } from '@nestjs/testing';
import { FibonacciService } from './fibonacci.service';

describe('FibonacciService', () => {
  let service: FibonacciService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FibonacciService],
    }).compile();

    service = module.get<FibonacciService>(FibonacciService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe.each([
    [3, '2'],
    [8, '21'],
    [15, '610'],
    [40, '102334155'],
    [50, '12586269025'],
    [100, '354224848179261915075'],
    [300, '222232244629420445529739893461909967206666939096499764990979600'],
  ])('for the %i-th Fibonacci number', (n, expected) => {
    it(`should return ${expected}`, () => {
      expect(service.getFibonacci(n)).toBe(expected);
    });
  });

  it('should throw an error for negative n', () => {
    expect(() => service.getFibonacci(-1)).toThrowError();
  });
});
