import { CacheModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import {
  makeCounterProvider,
  makeHistogramProvider,
} from '@willsoto/nestjs-prometheus';
import { FibonacciController } from './fibonacci.controller';
import { FibonacciService } from './fibonacci.service';
jest.mock('./fibonacci.service');

describe('FibonacciController', () => {
  let controller: FibonacciController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register({ ttl: 0, max: 100000 })],
      controllers: [FibonacciController],
      providers: [
        FibonacciService,
        makeHistogramProvider({
          name: 'request_duration',
          help: 'request latency in seconds',
        }),
        makeCounterProvider({
          name: 'error_counter',
          help: 'count all errors that occur',
        }),
      ],
    }).compile();

    controller = module.get<FibonacciController>(FibonacciController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call fibonacci service', () => {
    controller.fibonacci(3);
    expect(FibonacciService).toHaveBeenCalled();
  });
});
