import { CacheModule, Module } from '@nestjs/common';
import {
  makeCounterProvider,
  makeHistogramProvider,
  PrometheusModule,
} from '@willsoto/nestjs-prometheus';
import { FibonacciController } from './fibonacci.controller';
import { FibonacciService } from './fibonacci.service';

const CACHE_TTL = parseInt(process.env.CACHE_TTL) || 0;
const CACHE_MAX = parseInt(process.env.CACHE_MAX) || 100000;

@Module({
  imports: [
    PrometheusModule.register(),
    CacheModule.register({ ttl: CACHE_TTL, max: CACHE_MAX }),
  ],
  controllers: [FibonacciController],
  providers: [
    FibonacciService,
    makeHistogramProvider({
      name: 'request_duration',
      help: 'request latency in seconds',
    }),
    makeCounterProvider({
      name: 'error_counter',
      help: 'count all errors that occur',
    }),
  ],
})
export class FibonacciModule { }
