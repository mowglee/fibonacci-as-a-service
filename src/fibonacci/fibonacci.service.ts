import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export class FibonacciService {
  getFibonacci(n: number) {
    if (n < 0)
      throw new BadRequestException(
        'Fibonacci numbers are only defined for postive numbers of n.',
      );
    return this.fastDoubling(BigInt(n))[0].toString();
  }

  /**
   *  This function implements the fast doubling algorithm.
   *  https://www.nayuki.io/page/fast-fibonacci-algorithms
   * 
   * F(2i) = F(i) (2F(i+1) - F(i))
   * F(2i + 1) = F(i+1)^2 F(i)^2
   *
   * @param i the index of the Fibonacci number
   * @returns the tuple [F(i), F(i+1)]
   */
  private fastDoubling(i: bigint): bigint[] {
    if (i == 0n) return [0n, 1n];
    const [a, b] = this.fastDoubling(i / 2n);
    const c = a * (b * 2n - a);
    const d = a * a + b * b;
    if (i % 2n == 0n) return [c, d];
    return [d, c + d];
  }
}
